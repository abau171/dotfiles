# if not interactive, exit
[[ "$-" == *i* ]] || return

getContextPrefix() {
    if [ "$CONTEXT_NAME" != "" ]; then
        echo "$CONTEXT_NAME "
    fi
}
con() {
    bash --init-file <(echo ". ~/.bashrc; . ~/.con/$@")
}

export PS1='\[\e[32m\][\u@\h $(getContextPrefix)\w]\[\e[m\] '
export PROMPT_DIRTRIM=3

if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

# default editor
export VISUAL=vim
export EDITOR="$VISUAL"

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# BASHRC UPDATE COMMANDS
alias srcrc='source ~/.bashrc'
alias vimb='vim ~/.bashrc && srcrc'

# SYSTEM COMMANDS
alias ls='ls --color=auto'
alias ll='ls -alhF'
alias grep='grep --color=auto'
alias dud='du -h -d 1'

# PYTHON COMMANDS
alias activate='source ./env/bin/activate'
alias pr='time python run.py'
alias pt='python3 setup.py test'
alias pb='python setup.py build'
alias pup='pip install . --upgrade'
alias pie='pip install -e .'
alias pk='pkill python && fg'

# PATHS

if [ -d /usr/local/cuda ] ; then
    PATH="/usr/local/cuda/bin:$PATH"
    export LD_LIBRARY_PATH="/usr/local/cuda/lib64/:$LD_LIBRARY_PATH"
fi
if [ -d /opt/blender ] ; then
    PATH="/opt/blender:$PATH"
fi
if [ -d "$HOME"/work/dino ] ; then
    export DINO_ROOT="$HOME"/work/dino/
fi

# DEVELOPMENT

PATH="$PATH:build"
enterBuildDir() {
    if [ -d "build" ] ; then
        RETURN_FROM_BUILD_DIR=`pwd`
        cd build
    fi
}
exitBuildDir() {
    if [ $RETURN_FROM_BUILD_DIR ] && [ -d $RETURN_FROM_BUILD_DIR ] ; then
        cd $RETURN_FROM_BUILD_DIR
        unset RETURN_FROM_BUILD_DIR
    fi
}
mm() {
    enterBuildDir
    make -j `grep -c ^processor /proc/cpuinfo` "$@"
    local e=$?
    exitBuildDir
    return $e
}
alias mcm="mm clean && mm"
work() {
    cd ~/work/"$1"
}
crun() {
    g++ -march=native -O4 -o /tmp/$1.out --std=c++14 $1 && chmod +x /tmp/$1.out && /tmp/$1.out "${@:2}"
}
alias venv="source .venv/bin/activate"

# COMPLETION

_pycomp() {
    COMPREPLY=(`python3 ~/.pycomp.py $COMP_CWORD ${COMP_WORDS[*]}`);
}
pycomp() {
    complete -F _pycomp $1
}

pycomp work

# OTHER

alias todo="vim ~/.todo.txt"
alias pym='python3 -i -c "from math import *"'
alias get="sudo apt install"

# LOCAL
