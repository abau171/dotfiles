set nocompatible
set number
syntax on

" search settings
set hlsearch
set incsearch
set ignorecase
set smartcase " if uppercase is used, turns off ignorecase

set nobackup
set noswapfile
set nowritebackup

set undolevels=200

" Switch to 4 space indentation
function! UseSpacesToIndent()
  set nopaste
  set autoindent
  set softtabstop=4
  set expandtab
  set tabstop=4
  set shiftwidth=4
endfunction
command! UseSpacesToIndent call UseSpacesToIndent()

" Switch to tab indentation
function! UseTabsToIndent()
  set nopaste
  set autoindent
  set softtabstop=0
  set noexpandtab
  set tabstop=4
  set shiftwidth=4
endfunction
command! UseTabsToIndent call UseTabsToIndent()

call UseSpacesToIndent()

