#!/usr/bin/env bash

cp ~/.bashrc ~/.bashrc.save
cp .bashrc ~
cp .gitconfig ~
cp .vimrc ~
cp .pycomp.py ~

cd subl/
./install.sh
cd ..

