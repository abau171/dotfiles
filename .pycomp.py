import sys
import os

completeFunctions = dict()

def complete(command):
    def wrapper(f):
        global completeFunctions
        completeFunctions[command] = f
        return f
    return wrapper

### COMPLETION FUNCTIONS ###

@complete("work")
def work(i, args):

    if i > 0:
        return []

    prefix = args[i] if i < len(args) else ""
    allProjects = os.listdir(os.path.expanduser("~/work/"))

    projects = []
    for project in allProjects:
        if project.startswith(prefix):
            projects.append(project)

    return projects

### COMPLETION FUNCTIONS ###

if __name__ == "__main__":
    i = int(sys.argv[1]) - 1
    command = sys.argv[2]
    args = sys.argv[3:]
    try:
        print(" ".join(completeFunctions[command](i, args)))
    except KeyError:
        pass

